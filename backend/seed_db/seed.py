import json
import sys

from datetime import datetime
from typing import Type

from sqlalchemy import asc

# path to service module in container to seed db separately

sys.path.append("/app/backend")

from service.db.models import AuctionModel, BidModel, UserModel  # noqa: E402
from service.db.database import get_sqlalchemy_session  # noqa: E402


def seed_model(model: Type[AuctionModel | BidModel | UserModel], from_data: str):
    with open("seed_db/seed.json", "r") as file:
        data = json.load(file)
        model_data = data[from_data]
        for model_data_dict in model_data:
            model.create(**model_data_dict)


def seed_users():
    seed_model(UserModel, "users")


def seed_auctions():
    seed_model(AuctionModel, "auctions")

    kofein = AuctionModel.get_by_primary_key(id_=6)
    with get_sqlalchemy_session() as session:
        kofein.creation_time = datetime(2020, 9, 1)
        session.add(kofein)


def seed_bids():
    seed_model(BidModel, "bids")

    with get_sqlalchemy_session() as session:
        bids_auction_in_past = (
            session.query(BidModel).filter_by(auction_id=6).order_by(asc(BidModel.id))
        )
        for i, bid in enumerate(bids_auction_in_past, start=1):
            bid.date = datetime(2021, 3, i)
            session.add(bid)

        kytara = AuctionModel.get_by_primary_key(id_=1)
        kytara.set_leader("Jirik", 6200)

        bitcoin = AuctionModel.get_by_primary_key(id_=2)
        bitcoin.set_leader("Richardek", 520000)

        auto = AuctionModel.get_by_primary_key(id_=3)
        auto.set_leader("Tomasek", 503000)

        mobil = AuctionModel.get_by_primary_key(id_=4)
        mobil.set_leader("Tomasek", 15000)

        pikachu = AuctionModel.get_by_primary_key(id_=5)
        pikachu.set_leader("Jirik", 999999999)

        kofein = AuctionModel.get_by_primary_key(id_=6)
        kofein.set_leader("Adamek", 1000)


if __name__ == "__main__":
    seed_users()
    seed_auctions()
    seed_bids()
