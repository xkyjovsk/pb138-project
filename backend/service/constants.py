from enum import Enum


class Paths(str, Enum):
    auctions = "/api/auctions"
    auction = "/api/auction/{auction_id}"
    users_bids = "/api/user/{user_name}/auctions-bid"
    users_auctions = "/api/user/{user_name}/auctions"
    user_leading_auctions = "/api/user/{user_name}/leader"
    login = "/api/login"
    register = "/api/register"
    create_bid = "/api/bid"
    create_auction = "/api/auction"


class Sort(str, Enum):
    current = "current"
    past = "past"
    newest = "newest"
    oldest = "oldest"
    price_asc = "price-asc"
    price_desc = "price-desc"
    name_asc = "name-asc"
    name_desc = "name-desc"
