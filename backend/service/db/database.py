import logging
import os

from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker, Session

logger = logging.getLogger(__name__)


def get_pg_url() -> str:
    return (
        f"postgresql+psycopg2://{os.getenv('POSTGRES_USER')}"
        f":{os.getenv('POSTGRES_PASSWORD')}"
        f"@{os.getenv('POSTGRES_HOST')}"
        f":{os.getenv('POSTGRES_PORT', '5432')}"
        f"/{os.getenv('POSTGRES_DB')}"
    )


engine = create_engine(get_pg_url())
ScopedSession = scoped_session(sessionmaker(bind=engine))


@contextmanager
def get_sqlalchemy_session() -> Session:
    session = ScopedSession()
    try:
        yield session
        session.commit()
    except Exception as ex:
        logger.warning(f"Database error, exception thrown: {ex!r}")
        session.rollback()
        raise
