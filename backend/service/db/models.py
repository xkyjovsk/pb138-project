import logging

from datetime import datetime
from typing import TYPE_CHECKING, Optional, Iterable, Union

from sqlalchemy import Column, String, Integer, DateTime, Text, ForeignKey, asc, desc
from sqlalchemy.orm import declarative_base, relationship

from service.db.database import get_sqlalchemy_session

if TYPE_CHECKING:
    Base = object
else:
    Base = declarative_base()


logger = logging.getLogger(__name__)


class UserModel(Base):
    __tablename__ = "user"
    name = Column(String, primary_key=True)
    email = Column(String)
    # TODO: hash password
    password = Column(String)

    bids = relationship("BidModel", back_populates="user")

    def __repr__(self) -> str:
        return f"UserModel(name={self.name})"

    @classmethod
    def create(cls, name: str, email: str, password: str) -> "UserModel":
        with get_sqlalchemy_session() as session:
            user = cls()
            user.name = name
            user.email = email
            user.password = password
            session.add(user)
            return user

    @staticmethod
    def get_by_primary_key(name: str) -> Optional["UserModel"]:
        with get_sqlalchemy_session() as session:
            return session.query(UserModel).filter_by(name=name).first()

    @staticmethod
    def get_by_email(email: str) -> Optional["UserModel"]:
        with get_sqlalchemy_session() as session:
            return session.query(UserModel).filter_by(email=email).first()


class AuctionModel(Base):
    __tablename__ = "auction"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    product_info = Column(Text)
    creation_time = Column(DateTime, default=datetime.utcnow)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    start_price = Column(Integer)
    current_price = Column(Integer, default=start_price)
    picture_url = Column(String)

    author_name = Column(String, ForeignKey("user.name"))
    leader_name = Column(String, ForeignKey("user.name"))

    author = relationship(
        "UserModel", backref="auctions_created", foreign_keys=[author_name]
    )
    leader = relationship(
        "UserModel", backref="leader_of_auctions", foreign_keys=[leader_name]
    )
    bids = relationship("BidModel", back_populates="auction")

    def __repr__(self) -> str:
        return f"AuctionModel(id={self.id}, name={self.name})"

    @classmethod
    def create(
        cls,
        name: str,
        product_info: str,
        start_time: datetime,
        end_time: datetime,
        start_price: int,
        picture_url: str,
        author_name: str,
    ) -> "AuctionModel":
        with get_sqlalchemy_session() as session:
            auction = cls()
            auction.name = name
            auction.product_info = product_info
            auction.start_time = start_time
            auction.end_time = end_time
            auction.start_price = start_price
            auction.current_price = start_price
            auction.picture_url = picture_url
            auction.author_name = author_name
            session.add(auction)
            return auction

    @staticmethod
    def get_by_primary_key(id_: int) -> Optional["AuctionModel"]:
        with get_sqlalchemy_session() as session:
            return session.query(AuctionModel).filter_by(id=id_).first()

    @staticmethod
    def get_all() -> Optional[Iterable["AuctionModel"]]:
        with get_sqlalchemy_session() as session:
            return session.query(AuctionModel).all()

    @staticmethod
    def get_alive() -> Optional[Iterable["AuctionModel"]]:
        with get_sqlalchemy_session() as session:
            return (
                session.query(AuctionModel)
                .filter(datetime.utcnow() < AuctionModel.end_time)
                .all()
            )

    @staticmethod
    def get_past() -> Optional[Iterable["AuctionModel"]]:
        with get_sqlalchemy_session() as session:
            return (
                session.query(AuctionModel)
                .filter(AuctionModel.end_time < datetime.utcnow())
                .all()
            )

    @staticmethod
    def get_by_order(
        order: Union[desc, asc], param: Column[DateTime | Integer | String]
    ) -> Optional[Iterable["AuctionModel"]]:
        with get_sqlalchemy_session() as session:
            logger.debug(f"Getting auctions by order: {order} and param: {param}")
            return session.query(AuctionModel).order_by(order(param)).all()

    def set_leader(self, leader_name: str, new_price: int) -> None:
        with get_sqlalchemy_session() as session:
            logger.debug(
                f"Setting new leader={leader_name} with new price={new_price}"
                f" of auction id={self.id}"
            )
            self.leader_name = leader_name
            self.current_price = new_price
            session.add(self)


class BidModel(Base):
    __tablename__ = "bid"
    id = Column(Integer, primary_key=True)
    price = Column(Integer)
    date = Column(DateTime, default=datetime.utcnow)

    user_name = Column(String, ForeignKey("user.name"))
    auction_id = Column(Integer, ForeignKey("auction.id"))

    user = relationship("UserModel", back_populates="bids")
    auction = relationship("AuctionModel", back_populates="bids")

    def __repr__(self) -> str:
        return f"BidModel(id={self.id})"

    @classmethod
    def create(cls, price: int, user_name: str, auction_id: int) -> "BidModel":
        with get_sqlalchemy_session() as session:
            bid = cls()
            bid.price = price
            bid.user_name = user_name
            bid.auction_id = auction_id
            session.add(bid)
            return bid
