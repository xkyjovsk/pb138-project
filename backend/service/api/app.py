import logging

from http import HTTPStatus
from datetime import datetime
from typing import Iterable, Type, Union, Optional

from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

from pydantic import BaseModel
from sqlalchemy import asc, desc

from service.api.schema import (
    AuctionInputSchema,
    AuctionResponseSchema,
    BidSchema,
    RegisterSchema,
    UserResponseSchema,
    LoginSchema,
)
from service.db.models import AuctionModel, BidModel, UserModel
from service.constants import Paths, Sort


logger = logging.getLogger(__name__)
app = FastAPI()


# TODO: allow everything for dev - change this to _specific_ origins once project is ready in prod
origins = ["*"]
methods = [
    "GET",
    "POST",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=methods,
    allow_headers=["Access-Control-Allow-Origin"],
)


def make_response_of_multiple_models(
    schema: Type[AuctionResponseSchema | BidSchema | UserResponseSchema],
    models: Optional[Iterable[AuctionModel | BidModel | UserModel]],
) -> list[BaseModel]:
    result: list[BaseModel] = []
    # to please mypy
    if models is None:
        return result

    for model in models:
        result.append(schema.from_orm(model))

    return result


def asc_or_desc(sort: str) -> Union[asc, desc]:
    if sort in [Sort.newest, Sort.price_desc, Sort.name_desc]:
        return desc

    return asc


# separating these two functions just to please mypy later


def find_user_model(model_to_search: Type[UserModel], primary_key: str) -> UserModel:
    model = model_to_search.get_by_primary_key(primary_key)
    if not model:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND,
            detail=f"{model_to_search.__tablename__} unknown",
        )

    return model


def find_auction_model(
    model_to_search: Type[AuctionModel], primary_key: int
) -> AuctionModel:
    model = model_to_search.get_by_primary_key(primary_key)
    if not model:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND,
            detail=f"{model_to_search.__tablename__} unknown",
        )

    return model


@app.get(
    path=Paths.auctions,
    response_model=list[AuctionResponseSchema],
    status_code=HTTPStatus.OK,
)
def list_auctions(sort: str | None = None):
    logger.debug(f"Get list of auctions for sorting={sort}")
    match sort:
        case Sort.current:
            return make_response_of_multiple_models(
                AuctionResponseSchema, AuctionModel.get_alive()
            )
        case Sort.past:
            return make_response_of_multiple_models(
                AuctionResponseSchema, AuctionModel.get_past()
            )
        case Sort.newest | Sort.oldest:
            return make_response_of_multiple_models(
                AuctionResponseSchema,
                AuctionModel.get_by_order(
                    asc_or_desc(sort), AuctionModel.creation_time
                ),
            )
        case Sort.price_asc | Sort.price_desc:
            return make_response_of_multiple_models(
                AuctionResponseSchema,
                AuctionModel.get_by_order(
                    asc_or_desc(sort), AuctionModel.current_price
                ),
            )
        case Sort.name_asc | Sort.name_desc:
            return make_response_of_multiple_models(
                AuctionResponseSchema,
                AuctionModel.get_by_order(asc_or_desc(sort), AuctionModel.name),
            )
        case _:
            return make_response_of_multiple_models(
                AuctionResponseSchema, AuctionModel.get_all()
            )


@app.post(
    path=Paths.login,
    status_code=HTTPStatus.OK,
)
def login(login_schema: LoginSchema):
    user_model = find_user_model(UserModel, login_schema.name)
    if user_model.password != login_schema.password:
        raise HTTPException(status_code=HTTPStatus.UNAUTHORIZED, detail="bad password")

    return UserResponseSchema.from_orm(user_model)


@app.get(
    path=Paths.auction,
    response_model=AuctionResponseSchema,
    status_code=HTTPStatus.OK,
)
def get_auction(auction_id: int):
    auction_model = find_auction_model(AuctionModel, auction_id)
    return AuctionResponseSchema.from_orm(auction_model)


@app.get(
    path=Paths.users_bids,
    response_model=list[AuctionResponseSchema],
    status_code=HTTPStatus.OK,
)
def list_auctions_where_user_bid(user_name: str):
    user_model = find_user_model(UserModel, user_name)
    auctions_where_user_had_bid = set()
    for bid in user_model.bids:
        auctions_where_user_had_bid.add(bid.auction)

    return make_response_of_multiple_models(
        AuctionResponseSchema, list(auctions_where_user_had_bid)
    )


@app.get(
    path=Paths.users_auctions,
    response_model=list[AuctionResponseSchema],
    status_code=HTTPStatus.OK,
)
def list_users_created_auctions(user_name: str):
    user_model = find_user_model(UserModel, user_name)
    # mypy doesn't like sqlalchemy's `backref`
    return make_response_of_multiple_models(
        AuctionResponseSchema, user_model.auctions_created  # type: ignore
    )


@app.get(
    path=Paths.user_leading_auctions,
    response_model=list[AuctionResponseSchema],
    status_code=HTTPStatus.OK,
)
def list_users_leading_auctions(user_name: str):
    user_model = find_user_model(UserModel, user_name)
    # mypy doesn't like sqlalchemy's `backref`
    return make_response_of_multiple_models(
        AuctionResponseSchema, user_model.leader_of_auctions  # type: ignore
    )


@app.post(path=Paths.register, status_code=HTTPStatus.CREATED)
def register(user: RegisterSchema):
    possible_user_duplicate = UserModel.get_by_primary_key(user.name)
    if possible_user_duplicate:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST, detail="user already exists"
        )

    possible_email_duplicate = UserModel.get_by_email(user.email)
    if possible_email_duplicate:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST, detail="email already in use"
        )

    return UserResponseSchema.from_orm(UserModel.create(**user.dict()))


@app.post(
    path=Paths.create_bid,
    status_code=HTTPStatus.CREATED,
)
def create_bid(bid: BidSchema):
    auction_model = find_auction_model(AuctionModel, bid.auction_id)

    # just check whether user exists
    find_user_model(UserModel, bid.user_name)

    if datetime.utcnow() < auction_model.start_time:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST, detail="auction hasn't started yet"
        )

    if auction_model.end_time < datetime.utcnow():
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST, detail="auction has ended"
        )

    if bid.price < auction_model.start_price:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail="price cannot be lower that starting price",
        )

    bid_model = BidModel.create(bid.price, bid.user_name, bid.auction_id)
    is_first_bid = auction_model.start_price == auction_model.current_price
    if (
        auction_model.start_price <= bid.price and is_first_bid
    ) or auction_model.current_price < bid.price:
        logger.info(
            f"User {bid.user_name} has the highest bid {bid.price}"
            f" and will be set as a leader of auction id={bid.auction_id}"
        )
        auction_model.set_leader(bid.user_name, bid.price)

    return BidSchema.from_orm(bid_model)


@app.post(path=Paths.create_auction, status_code=HTTPStatus.CREATED)
def create_auction(auction: AuctionInputSchema):
    # just check whether user exists
    find_user_model(UserModel, auction.author_name)

    auction_model = AuctionModel.create(**auction.dict())
    return AuctionResponseSchema.from_orm(auction_model)
