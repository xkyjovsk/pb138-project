from datetime import datetime
from typing import Any, Optional

from pydantic import AnyUrl, BaseModel, validator, EmailStr, root_validator


class UserResponseSchema(BaseModel):
    name: str
    email: EmailStr

    class Config:
        orm_mode = True


class RegisterSchema(UserResponseSchema):
    password: str

    @validator("name")
    def check_name_not_empty(cls, name: str):
        if not name:
            raise ValueError("username can't be empty")

        return name


class LoginSchema(BaseModel):
    name: str
    password: str

    class Config:
        orm_mode = True


class AuctionInputSchema(BaseModel):
    name: str
    product_info: str
    start_time: datetime
    end_time: datetime
    start_price: int
    picture_url: AnyUrl
    author_name: str

    class Config:
        orm_mode = True

    @validator("start_time", "end_time")
    def must_be_in_future(cls, value: datetime) -> datetime:
        if value < datetime.utcnow():
            raise ValueError("auction start/end must be in the future")

        return value

    @root_validator
    def start_time_must_be_before_end_time(
        cls, values: dict[str, Any]
    ) -> dict[str, Any]:
        start_time = values.get("start_time")
        end_time = values.get("end_time")
        if start_time is not None and end_time is not None and end_time < start_time:
            raise ValueError("end of auction must be after start")

        return values


class AuctionResponseSchema(BaseModel):
    id: int
    name: str
    product_info: str
    start_time: datetime
    end_time: datetime
    start_price: int
    picture_url: AnyUrl
    author_name: str
    creation_time: datetime
    current_price: int
    leader_name: Optional[str]

    class Config:
        orm_mode = True


class BidSchema(BaseModel):
    price: int
    user_name: str
    auction_id: int

    class Config:
        orm_mode = True
