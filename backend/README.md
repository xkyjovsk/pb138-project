Jak spustit service:

- pokud spoustis service poprve, pak zadej do konzole: `$ make first-run-service`
  ... nainstaluje se databaze, service a udela se inicialni migrace.
  V `backend/alembic/version` vznikne jakysi onvy soubor. ten nemaz to je ok.
  Dal se spusti adminer, takze muzes koukat do databaze pomoci nej.
  Databaze se v tomto make targetu uz sama naseeduje s nejakyma zakladnima datama.
  Pokud se ti data zdaji moc vagni, muzes je upravit `backend/test/example_data.py`. Je tam
  classa ExampleData - muzes upravovat texty jednotlivych promennych (aby byl seed vice
  kosaty), ale na promenne jako takove nesahej - zavisi na nich testy.
  (pozn. nwm ale pro podman-compose tenhle makefile target wbc nefunguje takze na to se musi
  prijit proc :D)
- pokud spoustis service uz podruhe a vicekrat tak staci jen `$ make run-service` a spusti
  se service uz s predchozimi daty, ktere do ni byly ulozeny
- API bezi na adrese `0.0.0.0` a na portu `8000`
- pokud chces vypnout service, tak `$ make stop-service`
- Backend zatim bezi pouze pro developement, nikoliv pro produkci
- nepredpokladam, ale kdybys nahodou potreboval migrovat databazi, spust
  `make migrate-db CHANGE="My change"`, kde obsah promenne `CHANGE` bude nazev migrace.
- Pro otestovani celeho backendu slouzi prikaz: `make check-backend-in-container`. Pokud
  se vrtas v backendu tak, prosim, ho otestuj. Tyhle testy totiz zatim nejsou (mozna ani nebudou)
  integrovane do gitlab CI.
- Pokud objevis jakykoliv bug na strane backendu, vytvor issue a assigni me tam
- Dokumentace API je v `API_docs.md`
