from http import HTTPStatus

from fastapi import HTTPException
from fastapi.testclient import TestClient
from flexmock import flexmock

from test.conftest import compare_two_json_lists, get_result_json_list
from test.example_data import ExampleData

from service.api.app import (
    app,
    find_user_model,
    find_auction_model,
    make_response_of_multiple_models,
)
from service.api.schema import AuctionResponseSchema, UserResponseSchema
from service.constants import Paths
from service.db.models import AuctionModel, BidModel, UserModel


client = TestClient(app)


class TestApi:
    def test_make_response_of_multiple_models(self, clean_before_and_after, new_user):
        assert make_response_of_multiple_models(UserResponseSchema, [new_user]) == [
            {"name": ExampleData.user_name, "email": ExampleData.email}
        ]

    def test_find_user_model(self, clean_before_and_after, new_user):
        user = find_user_model(UserModel, ExampleData.user_name)
        assert user.name == ExampleData.user_name

        try:
            find_user_model(UserModel, ExampleData.different_user_name)
        except HTTPException:
            # correct
            return

        assert False

    def test_find_auction_model(self, clean_before_and_after, new_auction):
        auction_model = find_auction_model(AuctionModel, new_auction.id)
        assert auction_model.name == ExampleData.auction_name_a

        try:
            find_auction_model(AuctionModel, 132123123123)
        except HTTPException:
            # correct
            return

        assert False

    def test_register(self, clean_before_and_after):
        response = client.post(
            url=Paths.register,
            json={
                "name": ExampleData.user_name,
                "email": ExampleData.email,
                "password": ExampleData.password,
            },
        )
        assert response.status_code == HTTPStatus.CREATED
        user = UserModel.get_by_primary_key(ExampleData.user_name)
        assert user.name == ExampleData.user_name

        duplicate_name = client.post(
            url=Paths.register,
            json={
                "name": ExampleData.user_name,
                "email": ExampleData.different_email,
                "password": ExampleData.password,
            },
        )
        assert duplicate_name.status_code == HTTPStatus.BAD_REQUEST
        assert duplicate_name.json()["detail"] == "user already exists"

        duplicate_email = client.post(
            url=Paths.register,
            json={
                "name": ExampleData.different_user_name,
                "email": ExampleData.email,
                "password": ExampleData.password,
            },
        )
        assert duplicate_email.status_code == HTTPStatus.BAD_REQUEST
        assert duplicate_email.json()["detail"] == "email already in use"

    def test_login(self, clean_before_and_after):
        client.post(
            url=Paths.register,
            json={
                "name": ExampleData.user_name,
                "email": ExampleData.email,
                "password": ExampleData.password,
            },
        )
        response = client.post(
            url=Paths.login,
            json={
                "name": ExampleData.user_name,
                "password": ExampleData.password,
            },
        )
        assert response.status_code == HTTPStatus.OK

        bad_pswd = client.post(
            url=Paths.login,
            json={
                "name": ExampleData.user_name,
                "password": ExampleData.different_password,
            },
        )
        assert bad_pswd.status_code == HTTPStatus.UNAUTHORIZED

    def test_create_auction(self, clean_before_and_after, new_user):
        response = client.post(
            url=Paths.create_auction,
            json={
                "name": ExampleData.auction_name_a,
                "product_info": ExampleData.product_info,
                "start_time": str(ExampleData.first_date_in_future),
                "end_time": str(ExampleData.second_date_in_future),
                "start_price": ExampleData.medium_price,
                "picture_url": ExampleData.picture_url,
                "author_name": ExampleData.user_name,
            },
        )
        assert response.status_code == HTTPStatus.CREATED

        auctions = AuctionModel.get_all()
        assert len(auctions) == 1
        assert auctions[0].name == ExampleData.auction_name_a

    def test_list_auctions(self, clean_before_and_after, multiple_auctions):
        response = client.get(url=Paths.auctions)
        assert response.status_code == HTTPStatus.OK
        assert compare_two_json_lists(
            response.json(),
            get_result_json_list(AuctionResponseSchema, multiple_auctions),
        )

    def test_get_auction(self, clean_before_and_after, new_auction):
        response = client.get(url=Paths.auction.format(auction_id=new_auction.id))
        assert response.status_code == HTTPStatus.OK

    def test_create_bid(self, clean_before_and_after, new_user, new_auction):
        flexmock(AuctionModel).should_receive("set_leader").with_args(
            new_user.name, ExampleData.cheap_price
        ).once()

        response = client.post(
            url=Paths.create_bid,
            json={
                "price": ExampleData.cheap_price,
                "user_name": new_user.name,
                "auction_id": new_auction.id,
            },
        )
        assert response.status_code == HTTPStatus.CREATED

        small_bid = client.post(
            url=Paths.create_bid,
            json={
                "price": ExampleData.cheapest_price,
                "user_name": new_user.name,
                "auction_id": new_auction.id,
            },
        )
        assert small_bid.status_code == HTTPStatus.BAD_REQUEST
        assert small_bid.json()["detail"] == "price cannot be lower that starting price"

    def test_time_range_create_bid(
        self, clean_before_and_after, multiple_users, multiple_auctions
    ):
        user = multiple_users[0]
        future_auction = multiple_auctions[0]
        response = client.post(
            url=Paths.create_bid,
            json={
                "price": ExampleData.highest_price,
                "user_name": user.name,
                "auction_id": future_auction.id,
            },
        )
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert response.json()["detail"] == "auction hasn't started yet"

        past_auction = multiple_auctions[3]
        response = client.post(
            url=Paths.create_bid,
            json={
                "price": ExampleData.highest_price,
                "user_name": user.name,
                "auction_id": past_auction.id,
            },
        )
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert response.json()["detail"] == "auction has ended"

    def test_list_auctions_where_user_bid(
        self, clean_before_and_after, multiple_users, multiple_auctions
    ):
        user = multiple_users[0]
        BidModel.create(ExampleData.medium_price, user.name, multiple_auctions[0].id)
        BidModel.create(ExampleData.higher_price, user.name, multiple_auctions[0].id)

        response = client.get(url=Paths.users_bids.format(user_name=user.name))
        assert response.status_code == HTTPStatus.OK
        assert len(response.json()) == 1
        assert response.json()[0]["name"] == ExampleData.auction_name_a

        BidModel.create(ExampleData.higher_price, user.name, multiple_auctions[1].id)

        another_response = client.get(url=Paths.users_bids.format(user_name=user.name))
        assert another_response.status_code == HTTPStatus.OK
        assert len(another_response.json()) == 2
        assert compare_two_json_lists(
            another_response.json(),
            get_result_json_list(
                AuctionResponseSchema, [multiple_auctions[0], multiple_auctions[1]]
            ),
        )

    def test_list_users_created_auctions(
        self, clean_before_and_after, multiple_users, multiple_auctions
    ):
        user = multiple_users[0]
        response = client.get(url=Paths.users_auctions.format(user_name=user.name))
        assert len(response.json()) == 3
        assert compare_two_json_lists(
            response.json(),
            get_result_json_list(
                AuctionResponseSchema,
                [multiple_auctions[0], multiple_auctions[2], multiple_auctions[3]],
            ),
        )

    def test_list_users_leading_auctions(
        self, clean_before_and_after, multiple_users, multiple_auctions
    ):
        user = multiple_users[0]
        response = client.get(
            url=Paths.user_leading_auctions.format(user_name=user.name)
        )
        assert not response.json()

        multiple_auctions[0].set_leader(user.name, ExampleData.higher_price)
        response = client.get(
            url=Paths.user_leading_auctions.format(user_name=user.name)
        )
        assert len(response.json()) == 1

        multiple_auctions[2].set_leader(user.name, ExampleData.higher_price)
        multiple_auctions[3].set_leader(user.name, ExampleData.higher_price)
        response = client.get(
            url=Paths.user_leading_auctions.format(user_name=user.name)
        )
        assert len(response.json()) == 3
        assert compare_two_json_lists(
            response.json(),
            get_result_json_list(
                AuctionResponseSchema,
                [multiple_auctions[0], multiple_auctions[2], multiple_auctions[3]],
            ),
        )

        different_user = multiple_users[1]
        multiple_auctions[2].set_leader(different_user.name, ExampleData.highest_price)
        response = client.get(
            url=Paths.user_leading_auctions.format(user_name=user.name)
        )
        assert len(response.json()) == 2
        assert compare_two_json_lists(
            response.json(),
            get_result_json_list(
                AuctionResponseSchema, [multiple_auctions[0], multiple_auctions[3]]
            ),
        )
        response = client.get(
            url=Paths.user_leading_auctions.format(user_name=different_user.name)
        )
        assert len(response.json()) == 1
        assert compare_two_json_lists(
            response.json(),
            get_result_json_list(AuctionResponseSchema, [multiple_auctions[2]]),
        )
