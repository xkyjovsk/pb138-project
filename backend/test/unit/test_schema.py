import pytest

from http import HTTPStatus

from fastapi.testclient import TestClient

from test.example_data import ExampleData

from service.api.app import app
from service.constants import Paths


client = TestClient(app)


class TestValidators:
    def test_dont_register_empty_name(self, clean_before_and_after):
        empty_username = client.post(
            url=Paths.register,
            json={
                "name": "",
                "email": ExampleData.different_email,
                "password": ExampleData.password,
            },
        )
        assert empty_username.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
        assert empty_username.json()["detail"][0]["msg"] == "username can't be empty"

    @pytest.mark.parametrize(
        "start_time,end_time,msg",
        [
            pytest.param(
                ExampleData.first_date_in_past,
                ExampleData.second_date_in_future,
                "auction start/end must be in the future",
            ),
            pytest.param(
                ExampleData.second_date_in_future,
                ExampleData.second_date_in_past,
                "auction start/end must be in the future",
            ),
            pytest.param(
                ExampleData.second_date_in_future,
                ExampleData.first_date_in_future,
                "end of auction must be after start",
            ),
        ],
    )
    def test_dates_must_be_in_future(
        self, start_time, end_time, msg, clean_before_and_after, new_user
    ):
        response = client.post(
            url=Paths.create_auction,
            json={
                "name": ExampleData.auction_name_a,
                "product_info": ExampleData.product_info,
                "start_time": str(start_time),
                "end_time": str(end_time),
                "start_price": ExampleData.medium_price,
                "picture_url": ExampleData.picture_url,
                "author_name": ExampleData.user_name,
            },
        )
        assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
        assert response.json()["detail"][0]["msg"] == msg
