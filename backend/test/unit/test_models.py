from datetime import datetime, timedelta

from test.example_data import ExampleData

from service.db.models import AuctionModel, BidModel, UserModel


class TestUserModel:
    def test_create(self, clean_before_and_after, new_user):
        # this just have to pass just without error
        pass

    def test_get_by_primary_key(self, clean_before_and_after, multiple_users):
        user_model = UserModel.get_by_primary_key(ExampleData.user_name)
        assert user_model.name == ExampleData.user_name
        assert user_model.email == ExampleData.email

        different_user_model = UserModel.get_by_primary_key(
            ExampleData.different_user_name
        )
        assert different_user_model.name == ExampleData.different_user_name
        assert different_user_model.email == ExampleData.different_email

    def test_get_by_email(self, clean_before_and_after, multiple_users):
        user_model = UserModel.get_by_email(ExampleData.email)
        assert user_model.name == ExampleData.user_name
        assert user_model.email == ExampleData.email

        different_user_model = UserModel.get_by_email(ExampleData.different_email)
        assert different_user_model.name == ExampleData.different_user_name
        assert different_user_model.email == ExampleData.different_email


class TestAuctionModel:
    # ids of auctions goes up by one so in method `test_create` auction with id=1 is created
    # on the next method - auction id continues with id=2 and so on

    def test_create(self, clean_before_and_after, new_auction):
        assert datetime.utcnow() - new_auction.creation_time < timedelta(seconds=2)
        assert new_auction.current_price == new_auction.start_price

    def test_get_author(self, clean_before_and_after, multiple_auctions):
        auctions = AuctionModel.get_all()
        assert (
            auctions[1].author_name
            == auctions[1].author.name
            == ExampleData.different_user_name
        )

    def test_create_multiple_auctions(self, clean_before_and_after, multiple_auctions):
        auctions = AuctionModel.get_all()
        assert len(auctions) == 4
        assert auctions[0].name != auctions[1].name != auctions[2].name
        assert (
            auctions[0].author_name == auctions[0].author.name == ExampleData.user_name
        )
        assert auctions[0].name == ExampleData.auction_name_a
        assert auctions[0].product_info == ExampleData.product_info
        assert auctions[0].start_time == ExampleData.first_date_in_future
        assert auctions[0].end_time == ExampleData.second_date_in_future
        assert datetime.utcnow() - auctions[0].creation_time < timedelta(seconds=2)
        assert auctions[0].start_price == ExampleData.cheap_price
        assert (
            auctions[0].current_price
            == auctions[0].start_price
            == ExampleData.cheap_price
        )

        assert auctions[1].name == ExampleData.auction_name_b
        assert auctions[1].product_info == ExampleData.different_product_info
        assert (
            auctions[1].author_name
            == auctions[1].author.name
            == ExampleData.different_user_name
        )

        assert auctions[2].name == ExampleData.auction_name_c
        assert auctions[2].end_time == ExampleData.third_date_in_future
        assert auctions[2].picture_url == ExampleData.different_picture_url

    def test_get_alive(self, clean_before_and_after, multiple_auctions):
        auctions = AuctionModel.get_alive()
        assert len(auctions) == 3
        assert auctions[0].name == ExampleData.auction_name_a
        assert ExampleData.auction_name_d not in [auction.name for auction in auctions]

    def test_get_past(self, clean_before_and_after, multiple_auctions):
        auctions = AuctionModel.get_past()
        assert len(auctions) == 1
        assert auctions[0].name == ExampleData.auction_name_d

    def test_set_leader(self, clean_before_and_after, new_user, new_auction):
        new_auction.set_leader(
            leader_name=new_user.name, new_price=ExampleData.higher_price
        )
        assert new_auction.leader.name == new_user.name
        assert new_auction.current_price == ExampleData.higher_price
        assert new_user.leader_of_auctions == [new_auction]


class TestBidModel:
    def test_create(self, clean_before_and_after, new_user, new_auction):
        bid = BidModel.create(
            price=ExampleData.higher_price,
            user_name=new_user.name,
            auction_id=new_auction.id,
        )
        assert bid.user == new_user
        assert bid.auction == new_auction
        assert new_user.bids == new_auction.bids == [bid]
