from datetime import datetime


class ExampleData:
    user_name = "Jozko"
    different_user_name = "Postih"
    another_user_name = "Karel"
    email = "jozko@moj.com"
    different_email = "postih@nuty.jsem"
    another_email = "king69@xxx.com"
    wrong_email = "wrong_email"
    password = "password123"
    different_password = "pswd123"
    auction_name_a = "deka"
    auction_name_b = "lavor"
    auction_name_c = "hovno"
    auction_name_d = "rakosi"
    product_info = "some_info"
    different_product_info = "different_info"
    another_product_info = "another_product_info"
    yet_another_product_info = "yet_another_product_info"
    first_date_in_past = datetime(2005, 1, 1, 1, 1, 1, 1)
    second_date_in_past = datetime(2007, 1, 1, 1, 1, 1, 1)
    first_date_in_future = datetime(2035, 1, 1, 1, 1, 1, 1)
    second_date_in_future = datetime(2045, 1, 1, 1, 1, 1, 1)
    third_date_in_future = datetime(2055, 1, 1, 1, 1, 1, 1)
    cheapest_price = 1
    cheap_price = 12
    medium_price = 123
    higher_price = 1234
    highest_price = 12345
    picture_url = "https://www.seznam.cz/"
    different_picture_url = "https://www.google.com/"
