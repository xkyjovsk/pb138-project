import json
from typing import Type

import pytest

from service.api.app import make_response_of_multiple_models
from service.api.schema import (
    AuctionResponseSchema,
    BidSchema,
    UserResponseSchema,
)
from service.db.database import get_sqlalchemy_session
from service.db.models import AuctionModel, BidModel, UserModel
from test.example_data import ExampleData


def clean_db():
    with get_sqlalchemy_session() as session:
        # deleting depends on order - otherwise IntegrityError
        # since deleting happens only in test cases thus I haven't defined
        # ondelete=CASCADE and onupdate=CASCADE yet
        session.query(BidModel).delete()
        session.query(AuctionModel).delete()
        session.query(UserModel).delete()


@pytest.fixture()
def clean_before_and_after():
    clean_db()
    yield
    clean_db()


@pytest.fixture()
def new_user():
    yield UserModel.create(
        name=ExampleData.user_name,
        email=ExampleData.email,
        password=ExampleData.password,
    )


@pytest.fixture()
def multiple_users():
    yield [
        UserModel.create(
            name=ExampleData.user_name,
            email=ExampleData.email,
            password=ExampleData.password,
        ),
        UserModel.create(
            name=ExampleData.different_user_name,
            email=ExampleData.different_email,
            password=ExampleData.password,
        ),
        UserModel.create(
            name=ExampleData.another_user_name,
            email=ExampleData.another_email,
            password=ExampleData.password,
        ),
    ]


@pytest.fixture()
def new_auction(new_user):
    yield AuctionModel.create(
        name=ExampleData.auction_name_a,
        product_info=ExampleData.product_info,
        start_time=ExampleData.second_date_in_past,
        end_time=ExampleData.second_date_in_future,
        start_price=ExampleData.cheap_price,
        picture_url=ExampleData.picture_url,
        author_name=ExampleData.user_name,
    )


@pytest.fixture()
def multiple_auctions(multiple_users):
    yield [
        AuctionModel.create(
            name=ExampleData.auction_name_a,
            product_info=ExampleData.product_info,
            start_time=ExampleData.first_date_in_future,
            end_time=ExampleData.second_date_in_future,
            start_price=ExampleData.cheap_price,
            picture_url=ExampleData.picture_url,
            author_name=ExampleData.user_name,
        ),
        AuctionModel.create(
            name=ExampleData.auction_name_b,
            product_info=ExampleData.different_product_info,
            start_time=ExampleData.first_date_in_future,
            end_time=ExampleData.second_date_in_future,
            start_price=ExampleData.cheap_price,
            picture_url=ExampleData.picture_url,
            author_name=ExampleData.different_user_name,
        ),
        AuctionModel.create(
            name=ExampleData.auction_name_c,
            product_info=ExampleData.product_info,
            start_time=ExampleData.second_date_in_future,
            end_time=ExampleData.third_date_in_future,
            start_price=ExampleData.medium_price,
            picture_url=ExampleData.different_picture_url,
            author_name=ExampleData.user_name,
        ),
        AuctionModel.create(
            name=ExampleData.auction_name_d,
            product_info=ExampleData.product_info,
            start_time=ExampleData.first_date_in_past,
            end_time=ExampleData.second_date_in_past,
            start_price=ExampleData.medium_price,
            picture_url=ExampleData.different_picture_url,
            author_name=ExampleData.user_name,
        ),
    ]


def get_result_json_list(
    schema: Type[AuctionResponseSchema | BidSchema | UserResponseSchema],
    model_list: list[AuctionModel | BidModel | UserModel],
) -> list[dict[str, str]]:
    return [
        json.loads(model.json())  # type: ignore
        for model in make_response_of_multiple_models(schema, model_list)
    ]


def compare_two_json_lists(
    fst: list[dict[str, str]], snd: list[dict[str, str]]
) -> bool:
    fst.sort(key=lambda dict_: dict_["id"])
    snd.sort(key=lambda dict_: dict_["id"])

    for fst_json, snd_json in zip(fst, snd):
        if not sorted(fst_json.items()) == sorted(snd_json.items()):
            return False

    return True
