`GET /api/auctions`

- vraci seznam vsech aukci
- da se radit pomoci pridani parametru `sort` z vyberu hodnot:
  `current|past|newest|oldest|price-asc|price-desc|name-asc|name-desc`.
  Napr.: `GET /api/auctions?sort=prise-asc`
- return status pri uspechu: `OK`
- return data example pri uspechu:
  - ```
    [
        {
            "name": "dalsi kytara",
            "product_info": "kytara vole",
            "start_time": "2022-09-09T13:12:00",
            "end_time": "2023-09-09T13:12:00",
            "start_price": 12,
            "picture_url": "https://pythonguides.com/wp-content/uploads/2020/12/datetime-1024x325.png",
            "author_name": "Jozko",
            "id": 2,
            "creation_time": "2022-05-19T10:43:18.186610",
            "current_price": 123
        }
    ]
    ```

`POST /api/login`

- vraci jmeno uzivatele a mail, pokud byl `user_name` spravny
- return status pri uspechu: `OK`
- schema:
  - ```
    "name": str
    "password": str
    ```

`GET /api/auction/{auction_id}`

- vraci aukci
- return status pri uspechu: `OK`
- return data example pri uspechu:

  - ```
    {
         "name": "kytara",
         "product_info": "kytara vole",
         "start_time": "2022-09-09T13:12:00",
         "end_time": "2023-09-09T13:12:00",
         "start_price": 12,
         "picture_url": "https://pythonguides.com/wp-content/uploads/2020/12/datetime-1024x325.png",
         "author_name": "Jozko",
         "id": 1,
         "creation_time": "2022-05-19T09:53:13.207696",
         "current_price": 10000
    }
    ```

  ```

  ```

`GET /api/user/{user_name}/auctions-bid`

- vraci seznam aukci, kde dany uzivatel prihodil
- return status pri uspechu: `OK`
- return data example pri uspechu:
  - ```
    [
          {
      	  "name": "kytara",
      	  "product_info": "kytara vole",
      	  "start_time": "2022-09-09T13:12:00",
      	  "end_time": "2023-09-09T13:12:00",
      	  "start_price": 12,
      	  "picture_url": "https://pythonguides.com/wp-content/uploads/2020/12/datetime-1024x325.png",
      	  "author_name": "Jozko",
      	  "id": 1,
      	  "creation_time": "2022-05-19T09:53:13.207696",
      	  "current_price": 10000
          }
    ]
    ```

`GET /api/user/{user_name}/auctions`

- vraci seznam aukci, ktere dany uzivatel vlasni
- return status pri uspechu: `OK`
- return data example pri uspechu:
  - ```
    [
          {
      	  "name": "kytara",
      	  "product_info": "kytara vole",
      	  "start_time": "2022-09-09T13:12:00",
      	  "end_time": "2023-09-09T13:12:00",
      	  "start_price": 12,
      	  "picture_url": "https://pythonguides.com/wp-content/uploads/2020/12/datetime-1024x325.png",
      	  "author_name": "Jozko",
      	  "id": 1,
      	  "creation_time": "2022-05-19T09:53:13.207696",
      	  "current_price": 10000
          }
    ]
    ```

`GET /api/user/{user_name}/leader`

- vraci seznam aukci, kde dany uzivatel vyhrava
- return status pri uspechu: `OK`
- return data example pri uspechu:
  - ```
    [
          {
      	  "name": "kytara",
      	  "product_info": "kytara vole",
      	  "start_time": "2022-09-09T13:12:00",
      	  "end_time": "2023-09-09T13:12:00",
      	  "start_price": 12,
      	  "picture_url": "https://pythonguides.com/wp-content/uploads/2020/12/datetime-1024x325.png",
      	  "author_name": "Jozko",
      	  "id": 1,
      	  "creation_time": "2022-05-19T09:53:13.207696",
      	  "current_price": 10000
          }
    ]
    ```

`POST /api/register`

- ulozi do databaze noveho uzivatele, pokud jeste neexistuje (vcetne mailu)
- return status pri uspechu: `CREATED`
- return data pri uspechu jsou ta sama data, ktera poslal uzivatel a ulozila se do databaze.
  Tzn. ta sama data, pokud se nestane nejaka chyba pri ukladani do databaze.
- schema:
  - ```
    name: str
    email: str
    password: str
    ```

`POST /api/bid`

- vytvori prihoz na aukci. Pokud bude prihoz mensi, nez je aktualne nejvyssi castka, ma uzivatel
  smulu
- return status pri uspechu: `CREATED`
- return data pri uspechu jsou ta sama data, ktera poslal uzivatel a ulozila se do databaze.
  Tzn. ta sama data, pokud se nestane nejaka chyba pri ukladani do databaze.
- schema:
  - ```
    price: int
    user_name: str
    auction_id: int
    ```

`POST /api/auction`

- vytvori novou aukci
- return status pri uspechu: `CREATED`
- return data pri uspechu jsou ta sama data, ktera poslal uzivatel a ulozila se do databaze.
  Tzn. ta sama data, pokud se nestane nejaka chyba pri ukladani do databaze.
- schema:
  - ```
    name: str
    product_info: str
    start_time: datetime (musi byt v budoucnosti)
    end_time: datetime (musi byt pozdeji nez start_time)
    start_price: int
    picture_url: URL
    author_name: str (musi existovat)
    ```
