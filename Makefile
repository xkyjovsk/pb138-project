# toto jsou Jirkovy targety - nesahejte na ne! :D
# pridavat dalsi si samozrejme muzete dle libosti xD

ifeq ($(MAKECMDGOALS), check-backend-in-container-ci)
include backend/test/.env.test
endif

CONTAINER_ENGINE ?= $(shell command -v podman 2> /dev/null || echo docker)
COMPOSE ?= $(shell command -v podman-compose 2> /dev/null || echo docker-compose)
SERVICE_CONTAINER ?= service
DB_CONTAINER ?= db
ADMINER_CONTAINER ?= adminer
TEST_SERVICE_CONTAINER ?= test-service
TEST_DB_CONTAINER ?= test-db

# don't run this if you are trying to run service for the first time!
# use `first-run-service` instead
run-service:
	$(COMPOSE) up --build -d

# since `service` depends on `db` in compose.yaml - this will work
# so don't delete it there!
# run this only when you run the `service` app for the first time
# this will initialize database and migrate for the first time
# next time use the `run-service` target
first-run-service:
	$(COMPOSE) run $(SERVICE_CONTAINER) \
		alembic revision --autogenerate -m "Initial migration"
	$(COMPOSE) run $(SERVICE_CONTAINER) alembic upgrade head
	$(COMPOSE) up --build -d
	$(CONTAINER_ENGINE) exec $(SERVICE_CONTAINER) python seed_db/seed.py

stop-service:
	$(COMPOSE) down

check-backend-in-container-ci:
	$(COMPOSE) -f backend/test/compose.test.yaml up --build -d
	# database may need some additional time to start up
	sleep 2
	# build service image
	$(CONTAINER_ENGINE) build -f backend/test/Containerfile.test . -t $(TEST_SERVICE_CONTAINER)
	$(CONTAINER_ENGINE) run -d \
		--name $(TEST_SERVICE_CONTAINER) \
		--env POSTGRES_USER=$(POSTGRES_USER) \
		--env POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) \
		--env POSTGRES_HOST=$(POSTGRES_HOST) \
		--env POSTGRES_DB=$(POSTGRES_DB) \
		--env POSTGRES_PORT=$(POSTGRES_PORT) \
		-p 8765:8765 \
		--network=test_test-network \
		$(TEST_SERVICE_CONTAINER) \
		/bin/bash -c 'alembic revision --autogenerate -m "Initial migration" && \
		              alembic upgrade head && \
                      uvicorn service.api.app:app --host 0.0.0.0 --port 8765 --reload'
	# also the service needs some time to catch up
	sleep 2
	$(CONTAINER_ENGINE) exec -ti $(TEST_SERVICE_CONTAINER) /bin/bash -c "pytest -vvv test/"
	$(CONTAINER_ENGINE) stop $(TEST_SERVICE_CONTAINER)
	$(CONTAINER_ENGINE) rm $(TEST_SERVICE_CONTAINER)
	$(COMPOSE) -f backend/test/compose.test.yaml down


check-backend-in-container:
	make check-backend-in-container-ci || \
 		$(CONTAINER_ENGINE) stop $(TEST_DB_CONTAINER) $(TEST_SERVICE_CONTAINER); \
 		$(CONTAINER_ENGINE) rm $(TEST_DB_CONTAINER) $(TEST_SERVICE_CONTAINER)

migrate-db:
	$(COMPOSE) up --build --force-recreate -d $(SERVICE_CONTAINER)
	# as always - db needs some time to catch up
	sleep 2
	$(COMPOSE) run $(SERVICE_CONTAINER) \
		alembic revision --autogenerate -m "$(CHANGE)"
	sleep 2
	$(COMPOSE) run $(SERVICE_CONTAINER) alembic upgrade head
	$(COMPOSE) down
